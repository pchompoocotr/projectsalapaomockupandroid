package com.salapao;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatButton;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link HomeFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link HomeFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class HomeFragment extends Fragment {


    View.OnClickListener callTranslateActivity =  new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Intent translateAct = new Intent(getActivity(),LanguageActivity.class);
            startActivity(translateAct);
        }
    };
    View.OnClickListener callProductDetailActivity =  new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Intent productDetailAct = new Intent(getActivity(),ProductDetailActivity.class);
            startActivity(productDetailAct);
        }
    };
    public HomeFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment HomeFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static HomeFragment newInstance() {
        HomeFragment fragment = new HomeFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        AppCompatButton tanslateBtn = (AppCompatButton) view.findViewById(R.id.translateBtn);
        tanslateBtn.setOnClickListener(callTranslateActivity);
        ImageView cover = (ImageView) view.findViewById(R.id.cover);
        //cover.setOnClickListener(callProductDetailActivity);
        return view;
    }

}
