package com.salapao;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatButton;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;



public class AccountFragment extends Fragment {


    public AccountFragment() {
        // Required empty public constructor
    }


    View.OnClickListener callLoginActivity =  new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Intent loginAct = new Intent(getActivity(),LoginActivity.class);
            startActivity(loginAct);
        }
    };
    public static AccountFragment newInstance() {
        AccountFragment fragment = new AccountFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_account_notlogin, container, false);
        AppCompatButton loginBtn = (AppCompatButton) view.findViewById(R.id.loginBtn);
        loginBtn.setOnClickListener(callLoginActivity);
        return view;
    }

}
