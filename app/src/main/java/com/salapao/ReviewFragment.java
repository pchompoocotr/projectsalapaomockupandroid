package com.salapao;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ListView;

import com.salapao.adapter.ChatAdapter;
import com.salapao.viewmodel.ChatMessage;
import com.salapao.viewmodel.CommonMethods;

import java.util.ArrayList;
import java.util.Random;

public class ReviewFragment extends Fragment {

    private EditText msg_edittext;
    private String user1 = "khushi", user2 = "khushi1";
    private Random random;
    public static ArrayList<ChatMessage> chatlist;
    public static ChatAdapter chatAdapter;
    ListView msgListView;

    public ReviewFragment() {
        // Required empty public constructor
    }
    public static ReviewFragment newInstance() {
        ReviewFragment fragment = new ReviewFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_product_review, container, false);
        //random = new Random();
        //((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Chats");
        //msg_edittext = (EditText) view.findViewById(R.id.messageEditText);
        //msgListView = (ListView) view.findViewById(R.id.msgListView);
        //ImageButton sendButton = (ImageButton) view
        //        .findViewById(R.id.sendMessageButton);
        //sendButton.setOnClickListener(this);

        // ----Set autoscroll of listview when a new message arrives----//
        //msgListView.setTranscriptMode(ListView.TRANSCRIPT_MODE_ALWAYS_SCROLL);
        //msgListView.setStackFromBottom(true);

        //chatlist = new ArrayList<ChatMessage>();
        //chatAdapter = new ChatAdapter(getActivity(), chatlist);
        //msgListView.setAdapter(chatAdapter);
        return view;
    }

    public void sendTextMessage(View v) {
        String message = msg_edittext.getEditableText().toString();
        if (!message.equalsIgnoreCase("")) {
            final ChatMessage chatMessage = new ChatMessage(user1, user2,
                    message, "" + random.nextInt(1000), true);
            chatMessage.setMsgID();
            chatMessage.body = message;
            chatMessage.Date = CommonMethods.getCurrentDate();
            chatMessage.Time = CommonMethods.getCurrentTime();
            msg_edittext.setText("");
            chatAdapter.add(chatMessage);
            chatAdapter.notifyDataSetChanged();
        }
    }

    View.OnClickListener sendMessage = new View.OnClickListener() {
        @Override
        public void onClick(View view) {

        }
    };
}
