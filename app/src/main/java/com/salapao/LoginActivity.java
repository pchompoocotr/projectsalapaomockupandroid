package com.salapao;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatButton;
import android.view.View;
import android.widget.TextView;

public class LoginActivity extends AppCompatActivity {

    AppCompatButton loginBtn;
    TextView forgotTxt;
    TextView createAccountTxt;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        loginBtn = (AppCompatButton) findViewById(R.id.loginBtn);
        forgotTxt = (TextView) findViewById(R.id.forgotpasswordTxt);
        createAccountTxt = (TextView) findViewById(R.id.createAccountTxt);

        forgotTxt.setOnClickListener(callforgotActivity);
        createAccountTxt.setOnClickListener(callCreateAccountActivity);
    }

    protected void onResume(){
        super.onResume();

    }

    View.OnClickListener callforgotActivity =  new View.OnClickListener(){
        @Override
        public void onClick(View view) {
            Intent loginAct = new Intent(LoginActivity.this,ForgotPasswordActivity.class);
            startActivity(loginAct);
        }
    };

    View.OnClickListener callCreateAccountActivity =  new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Intent createAct = new Intent(LoginActivity.this,SignUpStep1Activity.class);
            startActivity(createAct);
        }
    };
}
