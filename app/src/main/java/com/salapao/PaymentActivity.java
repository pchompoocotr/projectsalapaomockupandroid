package com.salapao;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatTextView;
import android.view.View;
import android.widget.LinearLayout;

public class PaymentActivity extends AppCompatActivity {

    AppCompatTextView titleTxt;

    View titleBar;
    View payment;
    View payment1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment);
        titleBar = (View) findViewById(R.id.titleBar);
        titleTxt = (AppCompatTextView) titleBar.findViewById(R.id.title);
        payment = (View) findViewById(R.id.payment);
        payment1 = (View) findViewById(R.id.payment1);
        payment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent orderConfirm = new Intent(PaymentActivity.this,CreditActivity.class);
                startActivity(orderConfirm);
            }
        });
        payment1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent orderConfirm = new Intent(PaymentActivity.this,CreditActivity.class);
                startActivity(orderConfirm);
            }
        });
        titleTxt.setText("Payment Method");

    }
}
