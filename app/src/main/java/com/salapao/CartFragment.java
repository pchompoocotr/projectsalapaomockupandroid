package com.salapao;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatButton;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.uniquestudio.library.CircleCheckBox;


public class CartFragment extends Fragment {


    View cart1;
    View cart2;
    View cart3;
    View cart4;
    View cart5;
    CircleCheckBox checkBox1;
    CircleCheckBox checkBox2;
    CircleCheckBox checkBox3;
    CircleCheckBox checkBox4;
    CircleCheckBox checkBox5;

    TextView nextBtn;

    int check1 = 0;
    int check2 = 0;
    int check3 = 0;
    int check4 = 0;
    public CartFragment() {
        // Required empty public constructor
    }

    // TODO: Rename and change types and number of parameters
    public static CartFragment newInstance () {
        CartFragment fragment = new CartFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_cart1, container, false);
        cart1 = (View) view.findViewById(R.id.cart1);
        cart2 = (View) view.findViewById(R.id.cart2);
        cart3 = (View) view.findViewById(R.id.cart3);
        cart4 = (View) view.findViewById(R.id.cart4);
        cart5 = (View) view.findViewById(R.id.cart5);
        checkBox1 = (CircleCheckBox) cart1.findViewById(R.id.checked);
        checkBox2 = (CircleCheckBox) cart2.findViewById(R.id.checked);
        checkBox3 = (CircleCheckBox) cart3.findViewById(R.id.checked);
        checkBox4 = (CircleCheckBox) cart4.findViewById(R.id.checked);
        checkBox5 = (CircleCheckBox) cart5.findViewById(R.id.checked);
        nextBtn = (TextView)cart5.findViewById(R.id.nextBtn);

        cart1.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                // do something
                if(check1 == 0){
                    checkBox1.setVisibility(View.VISIBLE);
                    check1++;
                }else{
                    checkBox1.setVisibility(View.INVISIBLE);
                    check1--;
                }
            }
        });
        cart2.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                // do something
                if(check2 == 0){
                    checkBox2.setVisibility(View.VISIBLE);
                    check2++;
                }else{
                    checkBox2.setVisibility(View.INVISIBLE);
                    check2--;
                }
            }
        });
        cart3.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                // do something
                if(check3 == 0){
                    checkBox3.setVisibility(View.VISIBLE);
                    check3++;
                }else{
                    checkBox3.setVisibility(View.INVISIBLE);
                    check3--;
                }
            }
        });
        cart4.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                // do something
                if(check4 == 0){
                    checkBox4.setVisibility(View.VISIBLE);
                    check4++;
                }else{
                    checkBox4.setVisibility(View.INVISIBLE);
                    check4--;
                }
            }
        });
        cart5.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                // do something
                checkBox1.setVisibility(View.VISIBLE);
                checkBox2.setVisibility(View.VISIBLE);
                checkBox3.setVisibility(View.VISIBLE);
                checkBox4.setVisibility(View.VISIBLE);
                checkBox5.setVisibility(View.VISIBLE);
                check1 = 1;
                check2 = 1;
                check3 = 1;
                check4 = 1;


            }
        });
        nextBtn.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                Intent orderConfirm = new Intent(getActivity(),OrderActivity.class);
                startActivity(orderConfirm);
            }
        });
        return view;
    }
}
