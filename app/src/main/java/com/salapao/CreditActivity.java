package com.salapao;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatTextView;
import android.view.View;
import android.widget.TextView;

public class CreditActivity extends AppCompatActivity {

    TextView pay;
    AppCompatTextView title;
    View view;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_credit);
        view = (View) findViewById(R.id.titleBar);
        title = (AppCompatTextView)  view.findViewById(R.id.title);
        pay = (TextView) findViewById(R.id.pay);

        title.setText("Debit/Credit Card");

        pay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent orderConfirm = new Intent(CreditActivity.this,PaymentSuccessActivity.class);
                startActivity(orderConfirm);
            }
        });
    }
}
