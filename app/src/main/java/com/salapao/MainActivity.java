package com.salapao;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

import com.aurelhubert.ahbottomnavigation.AHBottomNavigation;
import com.aurelhubert.ahbottomnavigation.AHBottomNavigationAdapter;

public class MainActivity extends AppCompatActivity {

    public static AHBottomNavigation bottomNavigation;
    public static HomeFragment home;
    public static PromotionFragment promotion;
    public static MessageFragment message;
    public static CartFragment cart;
    public static AccountFragment account;
    public AHBottomNavigation.OnTabSelectedListener mOnNavigationItemSelectedListener
            = new AHBottomNavigation.OnTabSelectedListener() {

        @Override
        public boolean onTabSelected(int position, boolean wasSelected) {
            Fragment selectedFragment = null;
            switch (position) {
                case 0:
                    if(home == null){
                        home = HomeFragment.newInstance();
                    }
                    selectedFragment = home;
                    break;
                case 1:
                    if(promotion == null){
                        promotion = PromotionFragment.newInstance();
                    }
                    selectedFragment = promotion;
                    break;
                case 2:
                    if(message == null){
                        message = MessageFragment.newInstance();
                    }
                    selectedFragment = message;
                    break;
                case 3:
                    if(cart == null){
                        cart = CartFragment.newInstance();
                    }
                    selectedFragment = cart;
                    break;
                case 4:
                    if(account == null){
                        account = AccountFragment.newInstance();
                    }
                    selectedFragment = account;
                    break;
            }
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.content, selectedFragment);
            transaction.commit();
            return true;
        }

    };

    public static void setSelectPostion(int pos){
    }

    public static void setNumberBadgeNavigation(String text,int pos){
        bottomNavigation.setNotification(text,pos);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        bottomNavigation = (AHBottomNavigation) findViewById(R.id.navigation);
        onCreateNavigation();



        //Manually displaying the first fragment - one time only
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.content, HomeFragment.newInstance());
        transaction.commit();
    }

    private void onCreateNavigation(){



        AHBottomNavigationAdapter navigationAdapter = new AHBottomNavigationAdapter(this, R.menu.navigation);
        int[] tabColors = getApplicationContext().getResources().getIntArray(R.array.nav_tab_colors);

        // Set tab nav bottom is white
        navigationAdapter.setupWithBottomNavigation(bottomNavigation,tabColors);
        bottomNavigation.setColored(true);

        // Set inactive and active color
        bottomNavigation.setAccentColor(Color.RED);
        bottomNavigation.setInactiveColor(Color.GRAY);

        bottomNavigation.setNotificationBackgroundColor(Color.RED);
        bottomNavigation.setNotificationTextColor(Color.WHITE);
        bottomNavigation.setTitleState(AHBottomNavigation.TitleState.ALWAYS_HIDE);

        bottomNavigation.setNotification("4", 3);

        bottomNavigation.setOnTabSelectedListener(mOnNavigationItemSelectedListener);
        bottomNavigation.setCurrentItem(0);

    }

}
