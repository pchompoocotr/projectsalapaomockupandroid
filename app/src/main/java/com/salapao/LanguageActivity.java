package com.salapao;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatButton;
import android.view.View;

public class LanguageActivity extends AppCompatActivity {

    AppCompatButton okBtn;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_language);

        okBtn = (AppCompatButton) findViewById(R.id.okBtn);
        okBtn.setOnClickListener(callTranslateActivity);

    }

    View.OnClickListener callTranslateActivity =  new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            finish();
        }
    };
}
