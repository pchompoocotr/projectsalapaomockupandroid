package com.salapao;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatTextView;
import android.view.View;

public class OrderActivity extends AppCompatActivity {

    AppCompatButton confirmBtn;
    View titleBar;
    AppCompatTextView titleTxt;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_orderconfirm);
        titleBar = (View) findViewById(R.id.titlebar);
        titleTxt = (AppCompatTextView) titleBar.findViewById(R.id.title);
        titleTxt.setText("Order Confirmation");
        confirmBtn = (AppCompatButton) findViewById(R.id.conPayment);

        confirmBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent paymentAct = new Intent(OrderActivity.this,PaymentActivity.class);
                startActivity(paymentAct);
            }
        });
    }
}
