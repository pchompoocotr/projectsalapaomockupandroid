package com.salapao;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

public class PaymentFailedActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_fail);
    }
}
