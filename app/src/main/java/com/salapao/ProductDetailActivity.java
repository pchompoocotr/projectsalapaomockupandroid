package com.salapao;

import android.os.Bundle;
import android.support.v4.app.FragmentTabHost;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;


public class ProductDetailActivity extends AppCompatActivity implements View.OnClickListener{

    FragmentTabHost tabHost;
    ImageButton addBtn;
    ImageButton removeBtn;
    TextView qtyTxt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_detail);
        tabHost = (FragmentTabHost) findViewById(R.id.tabHost);
        addBtn = (ImageButton) findViewById(R.id.addBtn);
        removeBtn = (ImageButton) findViewById(R.id.removeBtn);
        qtyTxt = (TextView) findViewById(R.id.qtyTxt);

        tabHost.setup(this, getSupportFragmentManager(), R.id.realtabcontent);

        tabHost.addTab(tabHost.newTabSpec("Product Detail").setIndicator("Product Detail"),
                ProductDetailFragment.class, null);
        tabHost.addTab(tabHost.newTabSpec("Review").setIndicator("Review"),
                ReviewFragment.class, null);
    }


    @Override
    public void onClick(View view) {
        switch(view.getId()){
            case R.id.addBtn:{
                qtyTxt.setText(Integer.parseInt(qtyTxt.getText().toString())+1);
                break;
            }
            case R.id.removeBtn:{
                qtyTxt.setText(Integer.parseInt(qtyTxt.getText().toString())-1);
                break;
            }
        }
    }
}
