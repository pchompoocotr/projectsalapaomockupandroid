package com.salapao;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.view.View;

public class SignUpStep1Activity extends AppCompatActivity {

    AppCompatButton continueBtn;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup_step1);
        continueBtn = (AppCompatButton) findViewById(R.id.continueBtn);
        continueBtn.setOnClickListener(callStep2Activity);
    }

    View.OnClickListener callStep2Activity =  new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Intent step2Act = new Intent(SignUpStep1Activity.this,SignUpStep2Activity.class);
            startActivity(step2Act);
        }
    };
}
